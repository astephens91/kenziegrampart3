const express = require("express")
const fs = require("fs")
const multer = require("multer");
const path = require("path")
const pug = require("pug");

const app = express();

//Set Storage Engine
const storage = multer.diskStorage({
    
    destination: './public/uploads',
    filename: function(req, file, callback){
        const timestamp = Date.now()
        callback(null, file.fieldname + '-' + timestamp + path.extname(file.originalname))
    }
});


//Check File Type

function checkFileType(file, callback){
    //Allowed extensions
    const fileTypes = /jpeg|jpg|png|gif/;
    //Check Ext
    const extName = fileTypes.test(path.extname(file.originalname).toLowerCase());
    //Check mime
    const mimeType = fileTypes.test(file.mimetype)
    if(mimeType && extName){
        return callback(null, true);
    } else {
        callback("Error: Images Only")
    }
}


const upload = multer({
    storage: storage, 
    fileFilter: function(req, file, callback){
        checkFileType(file, callback);
    }
}).single('myFile');



//Public Folder
function displayImages(imgNames){
    let output = "";
    for(let nameIndex=0; nameIndex<imgNames.length; nameIndex++){
        let names = imgNames[nameIndex];
        output += `<img src='../uploads/${names}'>`
        console.log(output)
        
    }
    return output
    
}

let imageNames

 fs.readdir("./public/uploads", function(err, items) {
    imageNames = items.reverse()})



app.get('/', (req, res) => {

    fs.readdir("./public/uploads", function(err, items) {
    imageNames = items.reverse()})
    console.log(imageNames)
    res.render('index', {imageArray: imageNames})})

//PUG Set (templating)
app.set('views', './views')
app.set("view engine", "pug");


//Init app



app.use(express.static('./public'));
app.use(express.json());

const port = 3000;


app.listen(port, () => console.log(`Server started on port ${port}`))

//Post



app.post('/upload', upload, function (request, response, next) {  //end points aren't file paths
    // request.file is the \`myFile\` file
    // request.body will hold the text fields, if there were any 
    let uploaded_files; 
    uploaded_files = request.file.filename;
    console.log("Uploaded: " + request.file.filename);
    console.log(uploaded_files)
    imageNames.unshift(uploaded_files)  
    response.render("index2",{uploads: uploaded_files}) 
  });

  app.post('/latest', function (request, response, next) {
    fs.readdir("./public/uploads", function (err, items) {
        let images = [];
        let timeStamp = 0;
        let clientTimeStamp = request.body.timeOfFetchRequest
        console.log("This is clientTimeStamp " + clientTimeStamp)
        
    //   <This will have three things here.  I had an empty array, a timestamp variable that was set to 0, and I also had a clientTimeStamp variable that I used that took the last request of the body.(look up .after)>
      for (let i = 0; i < items.length; i++) {
        let modified = fs.statSync(`./public/uploads/${items[i]}` ).mtimeMs;
         console.log(modified)
//    <I used statSync here as well as mtimeMs with modified as the variable as you see in the if statement.>
        if (parseInt(modified) > parseInt(clientTimeStamp)) {
            images.push(items[i])
        }
        //   <You need to push your images into the empty array here>
        if (parseInt(modified) > parseInt(timeStamp)) {
            timeStamp = modified
        //   <This is where you will make the highest time stamp to be the new modified version>
        }
    }
      response.send({
        "images": images,
        "timestamp": timeStamp
        // <This is where you send the images and timestamp information back.  You will need to pass in the array and the timestamp variables>
      })
    })
   })