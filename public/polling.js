let timestamp = Date.now()
const imageContainer = document.querySelector("#image-container")
console.log(imageContainer)
let errorCount = 0;

function liveUpdate() {

    let interval = setInterval(function (){
        
        let stamp = {timeOfFetchRequest:timestamp}
        fetch("/latest", {
            method: "POST",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
,            },
            body: JSON.stringify(stamp)
        })
        
        .then(res => res.json())
        .then(result => {
            console.log(result)
            timestamp = result.timestamp

            result.images.forEach(photo => {
                let imgelement = document.createElement("img")
                imgelement.src = `uploads/${photo}`
                imageContainer.prepend(imgelement)
            })
        })
        .catch(err =>{
            errorCount++
            if(errorCount >1){
                console.log("Connection Lost, updates needed.")
                clearInterval(interval)
            }
        })
    }, 5000);
}

document.addEventListener("DOMContentLoaded", function(){
    liveUpdate();
})